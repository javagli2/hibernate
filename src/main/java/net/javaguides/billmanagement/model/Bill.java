package net.javaguides.billmanagement.model;

import javax.persistence.*;

/**
 * Bill
 * This is a model class represents a StanMagazynowy entity
 * @author Tomasz
 *
 */

//TODO: @Tomek i @Marcin nazwa tego mi się nie podoba, pomyślcie nad czymś lepszym
// - trochę biedna ta encja, nie brakuje nam tutaj sum, zniżek itp ?
// - dodajcie test jednostkowy, który jakieś dane nam będzie generował, użyjcie @Ignore, chcemy sobie to sami uruchamiać kiedy będzie potrzebne

@Entity
@Table(name = "bill")
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;


    public Bill() {
    }

    public Bill withId(int id) {
        setId(id);
        return this;
    }

    public Bill build() {
        //action
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
