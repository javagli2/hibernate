package net.javaguides.menumenagement.dao;

import net.javaguides.technical.dao.GenericDao;
import net.javaguides.menumenagement.model.Menu;

/**
 * CRUD database operations
 * @author Ramesh Fadatare
 *
 */

public class MenuDao extends GenericDao<Menu> {

    @Override
    public void save(Menu entity) {
        //TODO: @Przemek i @Sonia
        // - nie można zapisać drugiego i kolejnego menu jako aktualnego, prawda, że to ma sens
        // - nadpiszcie tą metodę, rzućcie wyjątkiem, gdy tak by się stało
        // - oczywiście dodajcie test jednostkowy
    }
}