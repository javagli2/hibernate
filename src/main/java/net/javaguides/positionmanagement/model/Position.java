package net.javaguides.positionmanagement.model;

import net.javaguides.productsmenagement.model.Product;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

//TODO: @Darek i @Wiktoria
// - dodajcie relację do produktów koniecznych do ugotowania potrawy
// - dodajcie relację do produktów opcjonalnych (np kwiatek do ozdoby)
// - zastanówcie się jak użyć tutaj "mappedBy" :-) wtedy zobaczycie o co tam tak naprawdę chodzi
// - dodajcie test jednostkowy, który jakieś dane nam będzie generował, użyjcie @Ignore, chcemy sobie to sami uruchamiać kiedy będzie potrzebne

//TODO: @Sonia i @Darek i @Wiktoria
// - dodajcie DAO, napiszcie metodę która zwróci wszystkie dostępne pozycje (takie, które mają wszyskie konieczne produkty do przygotowania potrawy), niech się nazywa "getAvailablePositions"
// - stwórzcie w podpakiecie menumanagement.service klasę MenuService, niech ta klasa ma metodę, która zwróci wszystkie dostępne pozycje (takie które można ugotować) dla obecnego menu

// TODO: @Przemek i @Sonia: zaimplementujcie relację zwrotną do Menu

@Entity
@Table(name="position")
public class Position {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected int id;

    @Column(name = "positionName1")
    protected String positionName1;

    @Column(name = "isDishOfTheDay")
    protected boolean isDishOfTheDay;

    @Column(name = "price")
    protected BigDecimal price;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "position_product_required",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "position_id"))
    protected List<Product> requiredProducts = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "position_product_optional",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "position_id"))
    protected List<Product> optionalProducts = new ArrayList<>();

    public Position() {

    }

    public Position withPositionName1(String positionName1){
        setPositionName1(positionName1);
        return this;
    }

    public Position withIsDishOfTheDay(boolean isDishOfTheDay ){
       setDishOfTheDay(isDishOfTheDay);
        return this;
    }

    public Position withPrice(BigDecimal price){
        setPrice(price);
        return this;
    }

    public Position withId(int id){
        setId(id);
        return this;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPositionName1() {
        return positionName1;
    }

    public void setPositionName1(String positionName1) {
        this.positionName1 = positionName1;
    }


    public boolean isDishOfTheDay() {
        return isDishOfTheDay;
    }

    public void setDishOfTheDay(boolean dishOfTheDay) {
        isDishOfTheDay = dishOfTheDay;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<Product> getRequiredProducts() {
        return requiredProducts;
    }

    public void setRequiredProducts(List<Product> requiredProducts) {
        this.requiredProducts = requiredProducts;
    }

    public List<Product> getOptionalProducts() {
        return optionalProducts;
    }

    public void setOptionalProducts(List<Product> optionalProducts) {
        this.optionalProducts = optionalProducts;
    }

    public boolean hasAllRequiredProducts() {
        boolean positionAvailable = true;

        for (Product product : getRequiredProducts()) {
            positionAvailable &= product.getStockLevel().getAmount() > 0;
        }

        return positionAvailable;
    }
}