package net.javaguides.positionmanagement.dao;

import net.javaguides.positionmanagement.model.Position;
import net.javaguides.technical.dao.GenericDao;
import net.javaguides.technical.utl.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class PositionDao extends GenericDao<Position> {
    public List<Position> getByName(String param) {
        Transaction transaction = null;
        List< Position > result = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an user object

            Query query = session.createQuery("from Position where positionName1 = :param1");
            query.setParameter("param1", param);

            result = query.getResultList();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return result;
    }
}
