package net.javaguides.customermanagement.service;

import net.javaguides.customermanagement.model.Customer;

import java.util.LinkedHashMap;
import java.util.Map;

public class CustomerService {
    public Customer getCurrentUser() {
        return null;
    }

    public String getWelcomeScreen(Customer customer) {
        return String.format("Dzień dobry %s. Witamy ponownie.", customer.getFirstName());
    }

    private static final Map<Integer, Integer> discountConfiguration = new LinkedHashMap();
    static{
        discountConfiguration.put(20, 10);
        discountConfiguration.put(5, 5);
        discountConfiguration.put(50, 15);
    }

    public int getDiscount(Customer user){
        int countOfOrders = user.getOrders().size();

        if(countOfOrders == 0){
            return 0;
        }

        Integer discount = discountConfiguration.entrySet().stream()
                .filter(e -> countOfOrders >= e.getKey())
                .max((o1, o2) -> o1.getKey()-o2.getKey())
                .get().getValue();

        return discount;
    }
}
