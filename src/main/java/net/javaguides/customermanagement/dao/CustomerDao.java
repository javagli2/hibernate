package net.javaguides.customermanagement.dao;

import net.javaguides.customermanagement.model.Customer;
import net.javaguides.technical.dao.GenericDao;

public class CustomerDao extends GenericDao<Customer> {
}
