package net.javaguides.customermanagement.model;

import net.javaguides.ordersmanagement.model.Order;
import net.javaguides.restaurantmanagement.model.Restaurant;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customers")
public class Customer {

    //TODO: @Ania i Kasia
    // - poproszę przerwy między polami, źle się czyta taki zlepek
    // - dobry typ relacji do Restaurant, brakuje relacji zwrotnej, będąc Restauracją chcemy wiedzieć, ilu mamy klientów
    // - zaimplementujcie DAO, dodajcie możliwość dodawania klientów, ale nie można drugi raz dodać takiego samego klienta, klienci mają być unikatowi.
    // - rzućcie wyjątkiem (który zdefiniujecie w podpakiecie .exception) jeśli będzie próba zapisu takiego samego klienta po raz drugi
    // - zaimplementujcie test jednostkowy, który to sprawdzi
    // - dodajcie test jednostkowy, który jakieś dane nam będzie generował, użyjcie @Ignore, chcemy sobie to sami uruchamiać kiedy będzie potrzebne

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @ManyToOne(cascade = CascadeType.ALL)
    private Restaurant restaurant;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Order> orders;

    public Customer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
