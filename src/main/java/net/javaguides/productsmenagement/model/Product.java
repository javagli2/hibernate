package net.javaguides.productsmenagement.model;

import net.javaguides.positionmanagement.model.Position;
import net.javaguides.stocklevelmanagment.model.StockLevel;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

//TODO: @Wiktoria i @Darek
// - checemy wiedzieć który produkt jest potrzebny do ugotowania i ozdobienia dania, brakuje relacji
// - dodajcie DAO, które powie nam jakich pozycji nie dostarczymy jeśli będzie nam produktu brakowało
// - dodajcie test jednostkowy, który jakieś dane nam będzie generował, użyjcie @Ignore, chcemy sobie to sami uruchamiać kiedy będzie potrzebne

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    protected int id;

    @Column(name = "name")
    protected String name;

    @Column(name = "productPrice")
    protected double productPrice;

    @OneToOne(cascade = CascadeType.ALL)
    protected StockLevel stockLevel;

    @ManyToMany(mappedBy = "requiredProducts", fetch = FetchType.EAGER)
    protected List<Position> positionsReq = new ArrayList<>();

    @ManyToMany(mappedBy = "optionalProducts")
    protected List<Position> positionsOpt = new ArrayList<>();

    public Product() {
    }


    public Product withId(int Id) {
        setId(id);
        return this;
    }

    public Product withName(String name) {
        setName(name);
        return this;
    }

    public Product withProductPrice(double productPrice) {
        setProductPrice(productPrice);
        return this;
    }

    public Product build() {
        return this;
    }


    public int getId() {
        return id;
    }

    public Product setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public Product setProductPrice(double productPrice) {
        this.productPrice = productPrice;
        return this;
    }

    public StockLevel getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(StockLevel stockLevel) {
        this.stockLevel = stockLevel;
    }

    public List<Position> getPositionsReq() {
        return positionsReq;
    }

    public void setPositionsReq(List<Position> positionsReq) {
        this.positionsReq = positionsReq;
    }

    public List<Position> getPositionsOpt() {
        return positionsOpt;
    }

    public void setPositionsOpt(List<Position> positionsOpt) {
        this.positionsOpt = positionsOpt;
    }
}
