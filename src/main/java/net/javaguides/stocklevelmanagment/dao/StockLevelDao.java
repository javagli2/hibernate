package net.javaguides.stocklevelmanagment.dao;

import net.javaguides.technical.dao.GenericDao;
import net.javaguides.stocklevelmanagment.model.StockLevel;
import net.javaguides.technical.utl.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

//TODO: @Marek i @Grzegorz
// - zaimplementujcie metodę, która dostarczy nam produkty których nam brakuje
// - takie które się kończą
// - takie które nam już długo leżą i się niedługo zepsują
// - trzeba pewnie będzie trochę rozszerzyć encję
public class StockLevelDao extends GenericDao<StockLevel> {
        public List<StockLevel> getByStockLevel(String param) {
            Transaction transaction = null;
            List< StockLevel > result = null;
            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                // start a transaction
                transaction = session.beginTransaction();
                // get an user object

                Query query = session.createQuery("from StockLevel where unit_desc = :param1");
                query.setParameter("param1", param);

                result = query.getResultList();

                // commit transaction
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                e.printStackTrace();
            }
            return result;
        }
}
