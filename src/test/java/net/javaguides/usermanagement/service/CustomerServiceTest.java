package net.javaguides.usermanagement.service;

import net.javaguides.customermanagement.model.Customer;
import net.javaguides.customermanagement.service.CustomerService;
import net.javaguides.menumenagement.model.Menu;
import net.javaguides.ordersmanagement.model.Order;
import net.javaguides.positionmanagement.model.Position;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CustomerServiceTest {
    CustomerService customerService = new CustomerService();

    @Test
    public void getNoDiscountIfUserHasLessThan5Orders() {

        //given
        Customer customer = Mockito.mock(Customer.class);
        List<Order> mockedOrders = getOrders(0);
        Mockito.when(customer.getOrders()).thenReturn(mockedOrders);

        //when
        int discount = customerService.getDiscount(customer);

        //then
        Assert.assertEquals(0, discount);
    }

    @Test
    public void get5PercentDiscountIfUserHas5To2Orders() {
        //given
        Customer customer = Mockito.mock(Customer.class);
        List<Order> mockedOrders = getOrders(5);

        Mockito.when(customer.getOrders()).thenReturn(mockedOrders);

        //when
        int discount = customerService.getDiscount(customer);

        //then
        Assert.assertEquals(5, discount);
    }

    @Test
    public void get10PercentDiscountIfUserHas20To50Orders() {
        //given
        Customer customer = Mockito.mock(Customer.class);
        List<Order> mockedOrders = getOrders(20);

        Mockito.when(customer.getOrders()).thenReturn(mockedOrders);

        //when
        int discount = customerService.getDiscount(customer);

        //then
        Assert.assertEquals(10, discount);
    }

    @Test
    public void get15PercentDiscountIfUserHasMoreThan50Orders() {
        //given
        Customer customer = Mockito.mock(Customer.class);
        List<Order> mockedOrders = getOrders(50);

        Mockito.when(customer.getOrders()).thenReturn(mockedOrders);

        //when
        int discount = customerService.getDiscount(customer);

        //then
        Assert.assertEquals(15, discount);
    }

    List<Position> menu(){
        Menu currentMenu = null;

        return currentMenu.getPositions().stream()
                .filter(Position::hasAllRequiredProducts)
                .collect(Collectors.toList());
    }

    @Test public void passingTest(){
        BigDecimal bd = new BigDecimal(-10);
        BigDecimal result = add10(bd);
    }

    BigDecimal add10(BigDecimal bigDecimal){
        return bigDecimal.add(new BigDecimal(10));
    }

    private List<Order> getOrders(int countOfOrders) {
        List<Order> mockedOrders = new ArrayList<>();
        for(int i=0;i<countOfOrders;i++){
            mockedOrders.add(new Order());
        }
        return mockedOrders;
    }

}
