package net.javaguides.helper;

import net.javaguides.customermanagement.dao.CustomerDao;
import net.javaguides.customermanagement.model.Customer;
import net.javaguides.positionmanagement.dao.PositionDao;
import net.javaguides.positionmanagement.model.Position;
import net.javaguides.productsmenagement.dao.ProductDao;
import net.javaguides.productsmenagement.model.Product;
import net.javaguides.restaurantmanagement.dao.RestaurantDao;
import net.javaguides.technical.dao.GenericDao;
import net.javaguides.restaurantmanagement.model.Restaurant;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataGenerator {

    @Test
    public void a_generateRestaurant() {
        GenericDao<Restaurant> dao = new GenericDao();

        Restaurant restaurant = new Restaurant();
        restaurant.setRestaurantName("Restauracja Łowicka");
        restaurant.setId(1);

        dao.save(restaurant);
    }

    @Test
    public void b_generateCustomers() {
        CustomerDao dao = new CustomerDao();
        RestaurantDao restaurantDao = new RestaurantDao();

        int count = 6;
        for(int i = 0; i<count; i++){
            Customer customer = new Customer();
            customer.setFirstName(String.format("Name%d", i));
            customer.setLastName(String.format("LastName%d", i));
            customer.setRestaurant(restaurantDao.get(1));
            dao.save(customer);
        }
    }

    @Test
    public void c_generateProducts(){
        ProductDao dao = new ProductDao();

        List<String> products = List.of("Maka", "Jajko", "Mleko", "Dzem", "Cukier puder", "Smietana");

        products.forEach(
                productName -> {
                    dao.save(new Product().withName(productName));
                }
        );
    }

    @Test
    public void d_generatePositions(){
        PositionDao positionDao = new PositionDao();
        ProductDao productDao = new ProductDao();

        Product maka = productDao.getByName("Maka").stream().findFirst().get();
        Product jajko = productDao.getByName("Jajko").stream().findFirst().get();
        Product smietana = productDao.getByName("Smietana").stream().findFirst().get();
        Product mleko = productDao.getByName("Mleko").stream().findFirst().get();
        Product dzem = productDao.getByName("Dzem").stream().findFirst().get();
        Product cukierPuder = productDao.getByName("Cukier puder").stream().findFirst().get();

        Position pierogi = new Position().withPositionName1("Pierogi");
        pierogi.setRequiredProducts(List.of(maka, jajko));
        pierogi.setOptionalProducts(List.of(smietana));
        positionDao.save(pierogi);

        Position nalesniki = new Position().withPositionName1("Nalesniki z dzemem");
        nalesniki.setRequiredProducts(List.of(maka, mleko, dzem));
        nalesniki.setOptionalProducts(List.of(cukierPuder));
        positionDao.save(nalesniki);
    }
}
